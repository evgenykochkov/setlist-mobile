package com.example.evgeny.setlist_mobile.data.entity

import com.example.evgeny.setlist_mobile.setlists.BaseModel

data class Coords(val coord_lat: String, val coord_long: String) : BaseModel() {

}