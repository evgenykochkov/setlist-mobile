package com.example.evgeny.setlist_mobile.utils

interface OnItemClickListener<T> {
    fun onItemClick(t: T)
}