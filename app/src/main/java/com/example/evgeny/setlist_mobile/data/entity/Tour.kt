package com.example.evgeny.setlist_mobile.data.entity

import com.example.evgeny.setlist_mobile.setlists.BaseModel

data class Tour(val name: String): BaseModel() {
}