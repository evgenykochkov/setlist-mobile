package com.example.evgeny.setlist_mobile.utils

interface AnswerListener<T> {
    fun getAnswer(t: T)
}